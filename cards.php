<?php
ini_set('display_errors', 0);
session_start();
if (!isset($_SESSION['sUserId'])) {
    header('Location: login');
}
$sUserId = $_SESSION['sUserId'];
$sData = file_get_contents('data/clients.json');
$jData = json_decode($sData);
if ($jData == null) {echo 'System update';}
$jInnerData = $jData->data;
$jClient = $jInnerData->$sUserId;
$jCards = $jClient->cards;

require_once 'top-user.php';
?>

<div class="client-profile">
  <div  class="box profile tab">
    <div>
      <h1 class="tab-title">Cards</h1>
        <table>
          <thead>
            <tr>
              <td>Card</td>
              <td>Type</td>
              <td>Created</td>
              <td>Active</td>
              <td></td>
            </tr>
          </thead>
          <tbody id="lblTransactions">
<?php

foreach ($jCards as $sKey => $jCard) {
  $jCardCreatedDate = date('d-M-Y', $jCard->dayCreated );
  $sActive = $jCard->active == 0 ? 'false' : 'true';
  $sIsActive= $jCard->active == 0 ? 'ACTIVATE':'BLOCK';
  echo "
            <tr>
              <td>$jCard->type</td>
              <td>$jCard->name</td>
              <td> $jCardCreatedDate DKK</td>
              <td><span class='$sActive dot'></span></td>
              <td> <a class='toggleLink' href='apis/api-cancell-card?cardId={$jCard->id}&phone={$jClient->phone}'>$sIsActive</a></td>
            </tr>";
}
?>
          </tbody>
        </table>
    </div>
  </div>
</div>
<?php
require_once 'bottom.php';
?>
