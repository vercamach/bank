<?php
ini_set('display_errors', 0);
session_start();
if (!isset($_SESSION['sUserId'])) {
    header('Location: login');
}
require_once 'top-user.php';
?>

<section class="page not-visible-on-load" id="request">
  <div class="form-wrapper" >
    <h1 class="title">REQUEST MONEY</h1>
    <form id="frmRequest">
      <input name="txtRequestFromPhone" id="txtRequestFromPhone" type="text" placeholder="request from phone" 
      data-validate="yes" data-type="string" data-min="8" data-max="8">

      <input name="txtRequestAmount" id="txtRequestAmount" type="number" placeholder="request amount" 
      data-validate="yes" data-type="integer" data-min="1">

      <input name="txtRequestMessage" id="txtRequestMessage" type="text" placeholder="request message" 
      data-validate="yes" data-type="string" data-min="2" data-max="10">

      <button>Request money</button>
    </form>
  </div>
</section>

<?php
$sLinkToScript = '<script src="js/request-money.js"></script>';
require_once 'bottom.php';
?>
