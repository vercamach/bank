function fnvGetBalance() {
  setInterval(function () {
    $.ajax({
      method: "GET",
      url: 'apis/api-get-balance',
      cache: false
    }).done(function (sBalance) {
      if (sBalance != $('#lblBalance').text()) {
        $('#lblBalance').text(sBalance)
      }
    }).fail(function () {})
  }, 10000)
}
fnvGetBalance()