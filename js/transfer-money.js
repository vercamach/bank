$('#frmTransfer').submit(function () {
    $.ajax({
        method: "GET",
        url: 'apis/api-transfer',
        data: {
            "phone": $('#txtTransferToPhone').val(),
            "amount": $('#txtTransferAmount').val(),
            "message": $('#txtTransferMessage').val()
        },
        cache: false,
        dataType: "JSON"
    }).
    done(function (jData) {
        if (jData.status == -1) {
            swal({
                title: "Oops",
                text: jData.message,
                icon: "warning"
            })
        }
        if (jData.status == 0) {
            swal({
                title: "Oops",
                text: jData.message,
                icon: "warning"
            })
        }

        if (jData.status == 1) {
            swal({
                title: "GOOD JOB",
                text: jData.message,
                icon: "success"
            }).then(function () {
                window.location = "profile";
            });
        }
    }).
    fail(function () {
        console.log('FATAL ERROR')
    })


    return false
})