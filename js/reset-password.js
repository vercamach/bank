$('#frmUpdateForgottenPassword').submit(function () {
    $.ajax({
        method: "POST",
        url: "apis/api-update-forgotten-password",
        data: $('#frmUpdateForgottenPassword').serialize(),
        dataType: "JSON"
    }).done(
        function (jData) {
            console.log(jData)
            if (jData.status == 1) {
                swal({
                    title: "YAY",
                    text: "Your password has been changed",
                    icon: "success"
                }).then(function () {
                    window.location = "profile";
                });
            } else {
                swal({
                    title: "Oops",
                    text: jData.message,
                    icon: "warning"
                });
            }
        }
    ).fail(function () {
        console.log('error')
    }
    )
    return false;
})