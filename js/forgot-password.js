$('#frmForgotPasswordSendLink').submit(function () {
    $.ajax({
            method: "GET",
            url: "apis/api-send-new-password-link",
            data: {
                'resetPhone': $('#txtForgotPasswordPhone').val()
            },
            dataType: "JSON",
            cache: false,
        })
        .done(function (jData) {
            if (jData.status == 0) {
                swal({
                    title: "Oops",
                    text: jData.message,
                    icon: "warning"
                })
            }
            if (jData.status == 1) {
                swal({
                    title: "Great",
                    text: "Go check your e-mail",
                    icon: "success"
                }).then(function () {
                    window.location = "login";
                })
            }
        })
        .fail(function () {
            console.log("error");
        });
    return false;
});