$('#frmGetCard').submit(function () {
    $.ajax({
        method: "POST",
        url: 'apis/api-get-card',
        data: $('#frmGetCard').serialize(),
        dataType: "JSON"
    }).done(
        function (jData) {
            swal({
                title: "Congrats",
                text: "You just got a card",
                icon: "success"
            }).then(function () {
                window.location = "cards";
            });
        }).fail()
    return false;
})