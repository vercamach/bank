$('#frmChangePassword').submit(function () {
    $.ajax({
        method: "POST",
        url: "apis/api-change-password",
        data: $('#frmChangePassword').serialize(),
        dataType: "JSON"
    }).done(
        function (jData) {
            if (jData.status == 1) {
                swal({
                    title: "YAY",
                    text: "Your password had been changed",
                    icon: "success"
                }).then(function () {
                    window.location = "profile";
                });
            } else {
                swal({
                    title: "Oops",
                    text: jData.message,
                    icon: "warning"
                });
            }
        }
    ).fail(function () {
        console.log('error')
    })
    return false;
})