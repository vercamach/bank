$('#frmApplyForAloan').submit(function () {
    $.ajax({
        method: "POST",
        url: 'apis/api-apply-for-a-loan',
        data: $('#frmApplyForAloan').serialize(),
        dataType: "JSON"
    }).done(
        function (jData) {
            if (jData.status == 1) {
                swal({
                    title: "Congrats",
                    text: "You just applied for a loan",
                    icon: "success"
                }).then(function () {
                    window.location = "loans";
                })
            }
            if (jData.status == 0) {
                swal({
                    title: "Oops",
                    text: jData.message,
                    icon: "warning"
                })
            }
        }).fail()
    return false;
})