

$("#frmSignup").submit(function () {
  $.ajax({
    method: "POST",
    url: "apis/api-signup",
    data: $("#frmSignup").serialize(),
    dataType: "JSON"
  })
    .done(function (jData) {
      //displays the response with errors
      console.log(jData);
      if (jData.status == 1) {
        swal({
          title: "You are signed up!",
          text: "Go check your email",
          icon: "success"
        }).then(function () {
          window.location = "login";
        });
      } else {
        swal({
          title: "Oops",
          text: jData.message,
          icon: "warning"
        });
      }
    })
    .fail(function () {
      console.log("error");
    });
  return false;
});
