$('#frmCreateAccount').submit(function () {
    $.ajax({
        method: "POST",
        url: 'apis/api-create-account',
        data: $('#frmCreateAccount').serialize(),
        dataType: "JSON"
    }).done(
        function (jData) {
            swal({
                title: "Congrats",
                text: "You just created an account",
                icon: "success"
            }).then(function () {
                window.location = "accounts";
            });
        }).fail(
        function () {
            console.log('error')
        })
    return false;
})