$('#frmRequest').submit(function () {
    $.ajax({
        method: "GET",
        url: 'apis/api-request-money',
        data: {
            "phone": $('#txtRequestFromPhone').val(),
            "amount": $('#txtRequestAmount').val(),
            "message": $('#txtRequestMessage').val()
        },
        dataType: "JSON",
        cache: false
    }).done(function (jData) {
        if (jData.status == -1) {
            swal({
                title: "Oops",
                text: jData.message,
                icon: "warning"
            })
        }
        if (jData.status == 0) {
            swal({
                title: "Oops",
                text: jData.message,
                icon: "warning"
            })
        }
        if (jData.status == 1) {
            swal({
                title: "GOOD JOB",
                text: "Request had been successful",
                icon: "success"
            }).then(function () {
                window.location = "profile";
            });
        }
    }).fail()


    return false;
})