$(document).ready(function () {
    $('.not-visible-on-load').hide()
    $('.navLink').click(function () {
        $('.navLink').removeClass('active')
        $('.page').hide()
        $(this).addClass('active')
        let sPageToShow = $(this).attr('data-showPage')
        $('#' + sPageToShow).show()
    })
})

// CONTACT ONE USER

$('#adminSendEmail').submit(function () {
    $.ajax({
            method: "POST",
            url: 'apis/api-admin-send-email',
            data: $('#adminSendEmail').serialize(),
            dataType: "JSON"
        })
        .done(
            function (jData) {
                if (jData.status == 1) {
                    swal({
                        title: "Great",
                        text: "Your E-mail was sent",
                        icon: "success"
                    }).then(function () {
                        window.location = "admin-profile";
                    });
                }
                if (jData.status == 0) {
                    swal({
                        title: "Oops",
                        text: jData.message,
                        icon: "warning"
                    })
                }
            })
        .fail(function () {
            console.log('error')
        })
    return false;
})

// CONTACT ALL USERS


$('#adminContactAll').submit(function () {
    $.ajax({
            method: "POST",
            url: 'apis/api-admin-contact-all-clients',
            data: $('#adminContactAll').serialize(),
            dataType: "JSON"
        })
        .done(
            function (jData) {
                if (jData.status == 1) {
                    swal({
                        title: "Great",
                        text: "E-mails were sent",
                        icon: "success"
                    }).then(function () {
                        window.location = "admin-profile";
                    });
                }
                if (jData.status == 0) {
                    swal({
                        title: "Oops",
                        text: jData.message,
                        icon: "warning"
                    })
                }
            })
        .fail(function () {
            console.log('error')
        })
    return false;
})

// TRANSFER MONEY
$('#adminTransfer').submit(function () {

    $.ajax({
        method: "GET",
        url: 'apis/api-admin-transfer',
        data: {
            "phone": $('#txtTransferTo').val(),
            "amount": $('#txtTransferAmount').val(),
            "message": $('#txtTransferMessage').val()
        },
        cache: false,
        dataType: "JSON"
    }).
    done(function (jData) {
        if (jData.status == -1) {
            swal({
                title: "Oops",
                text: jData.message,
                icon: "warning"
            })
        }
        if (jData.status == 0) {
            swal({
                title: "Oops",
                text: jData.message,
                icon: "warning"
            })
        }
        if (jData.status == 1) {
            swal({
                title: "GOOD JOB",
                text: "Transfer had feen successful",
                icon: "success"
            }).then(function () {
                window.location = "admin-profile";
            });
        }
    }).
    fail(function () {
        console.log('FATAL ERROR')
    })
    return false
})