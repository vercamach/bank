<?php

session_start();
if (!isset($_SESSION['sUserId'])) {
    header('Location: login');
}
$sUserId = $_SESSION['sUserId'];

$sData = file_get_contents('data/clients.json');
$jData = json_decode($sData);
if ($jData == null) {echo 'System update';}
$jInnerData = $jData->data;
$jClient = $jInnerData->$sUserId;


require_once 'top-user.php';
?>
<!--          *********** PROFILE ***************      -->
<div class="client-profile">
<h1 class="title">Profile</h1>

<div class="profile-flex">
<div class="profile tab person">
<img class="user" src="img/user-1.png" alt="user">
  <div  class="profileDiv" >
    <p><?=$jClient->name.' '.$jClient->lastName;?></p>
    <p>ID: <?=$jClient->id;?></p>
    <p>NAME: <?=$jClient->email;?></p>
    <p>PHONE: <?=$sUserId;?></p>
    <p>BALANCE:
     <span id="lblBalance"> <?=$jClient->balance;?></span> DKK
    </p>
    <a href="change-password">
        <div class="button">Change password</div>
    </a>
  </div>
</div>
<div class="profile tab">
      <a href="create-account"><div class="button">  Create an account</div></a>
      <a href="get-card"><div class="button">  Get a card</div></a>
      <a href="apply-for-a-loan"><div class="button">Apply for a loan</div></a>
  </div>
</div>

<!--        *********** INCOMING TRANSACTIONS ***************      -->
<div  class="box profile tab">
  <div id="transactions">
    <h1 class="tab-title">Deposits</h1>
      <table>
        <thead>
          <tr>
            <td>ID</td>
            <td>Date</td>
            <td>Amount</td>
            <td>From</td>
            <td>Message</td>
          </tr>
        </thead>
        <tbody id="lblTransactions">

      <?php
foreach ($jClient->incomingTransactions as $sTransactionId => $jTransaction) {
  $jTransactionCreatedDate = date('d-M-Y',  $jTransaction->date );
 
    echo "
          <tr>
            <td>$sTransactionId</td>
            <td>$jTransactionCreatedDate</td>
            <td class='deposit'>$jTransaction->amount DKK</td>
            <td>$jTransaction->fromPhone</td>
            <td>$jTransaction->message</td>
          </tr>
        ";}
?>
        </tbody>
      </table>
  </div>
</div>

<!--        *********** INCOMING TRANSACTIONS ***************      -->
<div  class="box profile tab">
  <div id="transactions">
    <h1 class="tab-title"> Transfers</h1>
      <table>
        <thead>
          <tr>
            <td>ID</td>
            <td>Date</td>
            <td>Amount</td>
            <td>To</td>
            <td>Message</td>
          </tr>
        </thead>
        <tbody id="lblTransactions">

      <?php
foreach ($jClient->outgoingTransactions as $sTransactionId => $jTransaction) {
  $jTransactionCreatedDate = date('d-M-Y',  $jTransaction->date );
 
    echo "
          <tr>
            <td>$sTransactionId</td>
            <td>$jTransactionCreatedDate</td>
            <td  class='transfer'>$jTransaction->amount DKK</td>
            <td>$jTransaction->fromPhone</td>
            <td>$jTransaction->message</td>
          </tr>
        ";}
?>
        </tbody>
      </table>
  </div>
</div>
</div>
<?php

$sLinkToScript = '<script src="js/profile.js"></script>';
require_once 'bottom.php';
?>
