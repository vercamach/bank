<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>VERO BANK</title>
    <link rel="stylesheet" href="css/app.css">
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
</head>
<body  class="basic">
    <div class="nav">
        <!-- <a href="index">
            <img class="logo" src="img/logo.png" alt="">
        </a> -->
        <div class="navContainer">
            <p class="navLink active" data-showPage="viewCustomers">View customers</p>
            <p class="navLink" data-showPage="sendEmail">Send email</p>
            <p class="navLink" data-showPage="contactAll">Contact all</p>
            <p class="navLink" data-showPage="transferMoney">Transfer money</p>
            <a class="navLink" href="profile">Client view</a>
            <a class="navLink logout" href="logout">Log out</a>
        </div>

    </div>