<?php

if(empty($_GET['resetPasswordPhone'])){sendResponse(0,__LINE__, 'Reset old password phone missing');}
$sActivationPhone= $_GET['resetPasswordPhone'];

if(empty ($_GET['resetPasswordKey']) ){sendResponse(0,__LINE__, 'Reset activation key missing');}
$sActivationKey = $_GET['resetPasswordKey'];

$sData = file_get_contents('data/clients.json');
$jData= json_decode($sData);
$jInnerData = $jData->data;

if(!$jInnerData->$sActivationPhone){sendResponse(0,__LINE__, 'Reset password phone is not in the database');}
if($jInnerData->$sActivationPhone->id != $sActivationKey){sendResponse(0,__LINE__, 'Reset password key is not in the database');}

function sendResponse($iStatus, $iLineNumber, $sMessage){
    echo '{"status":'.$iStatus.', "code":'.$iLineNumber.',"message":"'.$sMessage.'"}';
    exit;
}
 
require_once 'top-basic.php'
?>

<section>
  <div class="form-wrapper">
  <h1 class="title">Update password</h1>
    <form id="frmUpdateForgottenPassword" action="apis/api-update-forgotten-password" method="POST">
      <input name="txtResetPasswordPhone"  type="hidden" placeholder="phone" value=<?=$sActivationPhone?>>
      <input name="txtNewResetPassword" type="password" placeholder="new password"
      data-validate="yes"  data-type="string" data-min="4" data-max="50">
      <input name="txtNewConfirmPassword" type="password" placeholder="confirm new password"
      data-validate="yes"  data-type="string" data-min="4" data-max="50">
      <button>Update password </button>
    </form>
  </div>
</section>

<?php
$sLinkToScript = '<script src="js/reset-password.js"></script>';
require_once 'bottom.php'
?>