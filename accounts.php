<?php
ini_set('display_errors', 0);
session_start();
if (!isset($_SESSION['sUserId'])) {
    header('Location: login');
}
$sUserId = $_SESSION['sUserId'];
$sData = file_get_contents('data/clients.json');
$jData = json_decode($sData);
if ($jData == null) {echo 'System update';}
$jInnerData = $jData->data;
$jClient = $jInnerData->$sUserId;
$jAccounts = $jClient->accounts;
require_once 'top-user.php';
?>
<div class="client-profile">
  <div  class="box profile tab">
    <div id="transactions">
      <h1 class="tab-title">Accounts</h1>
        <table>
          <thead>
            <tr>
              <td>Name</td>
              <td>Balance</td>
              <td>Currency</td>
              <td>Created</td>
            </tr>
          </thead>
          <tbody id="lblTransactions">
<?php

  foreach ($jAccounts as $sKey => $jAccount) {
    $jAccountCreatedDate = date('d-M-Y', $jAccount->dayOpened );
      echo "
      <tr>
      <td>$jAccount->name</td>
      <td>$jAccount->balance</td>
      <td>$jAccount->currency</td>
      <td> $jAccountCreatedDate DKK</td>
    </tr>";
  }
?>
          </tbody>
        </table>
    </div>
  </div>
</div>

<?php
require_once 'bottom.php';
?>
