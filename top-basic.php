<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>VERO BANK</title>
    <link rel="stylesheet" href="css/app.css">
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
</head>
<body class="basic">
    <div class="nav">
        <!-- <a href="index">
           <h1 class="logo">VERO</h1>
        </a> -->
        <div class="navContainer">
            <a class="navLink" href="signup">Sign up</a>
            <a class="navLink" href="login">Log in</a> 
        </div>
    </div>