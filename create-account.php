<?php
session_start();

if( empty($_SESSION['sUserId'] ) ){
  header('Location: login');
  };
  

require_once 'top-user.php';?>
<section>
  <div class="form-wrapper">
  <h1 class="title">Create Account</h1>
    <form id="frmCreateAccount" action="apis/api-create-account" method="POST">
      <div class="icon-binder">
        <select name="txtAccountName" id="txtAccountName" class="required">
          <option value="Checking account">Checking account</option>
          <option value="Savings account">Savings account</option>
          <option value="Retirement Account">Retirement Account</option>
        </select><img class="arrow" src="img/down-arrow.png" alt="arrow">
      </div>
      <div class="icon-binder">
        <select name="txtSelectCurrency" id="txtSelectCurrency" class="required">
          <option value="DKK">DKK</option>
          <option  value="USD">USD</option>
        </select><img class="arrow" src="img/down-arrow.png" alt="arrow">
      </div>
      <button>Create Account</button>
    </form>
    <a href="profile">
    <p><- back to profile </p>
    </a>
  </div>
</section>

<?php 
$sLinkToScript =  '<script src="js/create-account.js"></script>';
require_once 'bottom.php';?>