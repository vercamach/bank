<?php require_once 'top-basic.php'; ?>
<section>
    <div class="form-wrapper signup">
    <h1 class="title">Sign up</h1>
        <form action="apis/api-signup"  id="frmSignup" method="POST">

            <input name="txtSignupName" type="text" placeholder="name"
            data-validate="yes" data-type="string" data-min="2" data-max="10"  >
        
            <input name="txtSignupLastName" type="text" placeholder="last name"
            data-validate="yes" data-type="string" data-min="2" data-max="10"  >
        
            <input name="txtSignupPhone" type="text" placeholder="phone"
            data-validate="yes" data-type="string" data-min="8" data-max="8">
        
            <input name="txtSignupEmail" type="text" placeholder="email"
            data-validate="yes" data-type="email"  data-min="5" data-max="40" >
        
            <input name="txtSignupPassword" type="password" placeholder="password"
            data-validate="yes" data-type="string"  data-min="4" data-max="40">
        
            <input name="txtSignupConfirmPassword" type="password" placeholder="confirm password"
            data-validate="yes" data-type="string"  data-min="4" data-max="40">
        
            <button>sign up</button>
        </form>
    </div>
</section>
<?php 
$sLinkToScript= '<script src="js/signup.js"></script>';
require_once 'bottom.php'; ?>

