<?php
ini_set('display_errors', 0);
session_start();
if (!isset($_SESSION['sUserId'])) {
    header('Location: login');
}
$sUserId = $_SESSION['sUserId'];
$sData = file_get_contents('data/clients.json');
$jData = json_decode($sData);
if ($jData == null) {echo 'System update';}
$jInnerData = $jData->data;
$jClient = $jInnerData->$sUserId;
$jLoans = $jClient->loans;

require_once 'top-user.php';
?>
<div class="client-profile">
  <div  class="box profile tab">
    <div id="transactions">
      <h1 class="tab-title">Loans</h1>
      <table>
        <thead>
          <tr>
            <td>Name</td>
            <td>Amount</td>
            <td>Approved</td>
          </tr>
        </thead>
        <tbody id="lblTransactions">
<?php
foreach ($jLoans as $sKey => $jLoan) {
  $sApproved = $jLoan->loanApproved == 0 ? 'false' : 'true';
    echo "
            <tr>
              <td>$jLoan->name</td>
              <td>$jLoan->loanAmount DKK</td>
             <td><span class='$sApproved dot'></span></td>
            </tr>";
}
?>
          </tbody>
        </table>
     </div>
  </div>
</div>

<?php
require_once 'bottom.php';
?>
