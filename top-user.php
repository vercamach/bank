<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>VERO BANK</title>
    <link rel="stylesheet" href="css/app.css">
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
</head>
<body class="basic">
    <div class="nav">
        <!-- <a href="index">
            <img class="logo" src="img/logo.png" alt="logo">
        </a> -->
        <div class="navContainer">

            <a class="navLink" href="profile">Profile</a>
            <a class="navLink" href="transfer-money">Transfer money</a>
            <a class="navLink" href="request-money">Request money</a>
            <a class="navLink" href="accounts">Accounts</a>
            <a class="navLink" href="cards">Cards</a>
            <a class="navLink" href="loans">Loans</a>
            <a class="navLink" href="admin-profile">Admin view</a>
            <a class="navLink logout" href="logout">Log out</a>
        </div>
    </div>