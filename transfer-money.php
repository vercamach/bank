<?php
ini_set('display_errors', 0);
session_start();
if (!isset($_SESSION['sUserId'])) {
    header('Location: login');
}
require_once 'top-user.php';

?>

<section id="transfer">
  <div class="form-wrapper">
    <h1 class="title">TRANSFER MONEY</h1>
    <form id="frmTransfer">
      <input name="txtTransferToPhone" id="txtTransferToPhone" type="text" placeholder="transfer to phone"
      data-validate="yes" data-type="string" data-max="8" data-min="8" >
  
      <input name="txtTransferAmount" id="txtTransferAmount" type="number" placeholder="transfer amount"
       data-validate="yes" data-type="integer" data-min="1">
  
      <input name="txtTransferMessage" id="txtTransferMessage" type="text" placeholder="transfer message"
       data-validate="yes" data-type="string" data-min="2" data-max="20">
      <button>Transfer money</button>
    </form>
  </div>
</section>

<?php

$sLinkToScript = '<script src="js/transfer-money.js"></script>';

require_once 'bottom.php';
?>
