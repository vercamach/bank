<?php
require_once 'top-basic.php'?>


<section>
  <div class="form-wrapper">
    <h1 class="title">Login</h1>
    <form id="frmLogin" method="POST">
      <div>
        <img class="loginProfile" src="img/phone.png" alt="icon">
        <input class="login" name="txtLoginPhone" placeholder="phone" type="text"
        data-validate="yes" data-type="string" data-min="8" data-max="8">
      </div>
      <div>
        <img class="loginProfile" src="img/lock.png" alt="icon">
        <input class="login" name="txtLoginPassword" placeholder="password" type="password"
        data-validate="yes" data-type="string" data-min="4" data-max="50">
      </div>
      <button class="button">login</button>
    </form>
    <div class="centerLink"><a href="forgot-password">Forgot password?</a></div>
  </div>
</section>


<?php
$sLinkToScript = '<script src="js/login.js"></script>';
require_once 'bottom.php'
?>