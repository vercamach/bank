<?php

session_start();
if (!isset($_SESSION['sUserId'])) {
    header('Location: login');
}
$sUserId = $_SESSION['sUserId'];

$sData = file_get_contents('data/clients.json');
$jData = json_decode($sData);
if ($jData == null) {echo 'System update';}
$jInnerData = $jData->data;
$jClient = $jInnerData->$sUserId;
$jLoans = $jClient->loans;

require_once 'top-user.php';

?>
<section>
   <div class="form-wrapper">
        <div>
            <h1 class="title">Change password</h1>
            <form id="frmChangePassword" action="apis/api-change-password" method="POST">
                <input name="txtOldPassword" type="password" placeholder="old password">

                <input name="txtNewPassword" type="password" placeholder="new password"
                data-validate="yes" data-type="string" data-min="4" data-max="50">

                <input name="txtNewConfirmPassword" type="password" placeholder="confirm new password"
                data-validate="yes" data-type="string" data-min="4" data-max="50">
                <button>Change password</button>
            </form>
            <div>
                 <a href="profile"><- Go back to profile</a>
            </div>
        </div>
    </div>
</section>
<?php

$sLinkToScript = '<script src="js/change-password.js"></script>';
require_once 'bottom.php';
?>
