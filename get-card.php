<?php
session_start();

if( empty($_SESSION['sUserId'] ) ){
  header('Location: login');
  };
require_once 'top-user.php';
?>

<section>
  <div class="form-wrapper">
  <h1 class="title">Get Card</h1>
    <form id="frmGetCard" action="apis/api-get-card" method="POST">
      <div class="icon-binder">
        <select name="txtCardType" id="txtCardType" class="required">
          <option value="VISA">VISA</option>
          <option value="MasterCard">MasterCard</option>
        </select>
        <img class="arrow" src="img/down-arrow.png" alt="arrow">
      </div>
      <div class="icon-binder">
        <select name="txtCardName" id="txtCardType"  class="required">
          <option value="Credit card">Credit card</option>
          <option value="Debit card">Debit card</option>
          <option value="ATM card">ATM card</option>
        </select>
        <img class="arrow" src="img/down-arrow.png" alt="arrow">
      </div>
      <button>Get card</button>
    </form>
    <a href="profile">
      <p><- back to profile </p>
    </a>
  </div>
</section>

<?php 
$sLinkToScript = '<script src="js/get-card.js"></script>';
require_once 'bottom.php';?>