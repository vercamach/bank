<?php
ini_set('display_errors', 0 );
session_start();

if( empty($_SESSION['sUserId'] ) ){
  header('Location: login');
  };
require_once 'top-user.php';
?>

<section>
  <div class="form-wrapper">
    <h1 class="title">Apply for a Loan</h1>
    <form id="frmApplyForAloan"  action="apis/api-apply-for-a-loan" method="POST">
      <div class="icon-binder">
        <select name="txtLoanType" id="txtLoanType">
          <option value="Mortgage">Mortgage</option>
          <option value="Student Loan">Student Loan</option>
          <option value="Personal Loan">Personal Loan</option>
          <option value="Car loan">Car loan</option>
        </select>
        <img class="arrow" src="img/down-arrow.png" alt="arrow">
      </div>
      <input name="txtLoanAmount" type="number" placeholder="amount"
      data-validate="yes" data-type="integer" data-min="1" >
      <button> Apply for a loan</button>
    </form>
    <a class="centerLink" href="profile">
      <p ><- back to profile </p>
    </a>
  </div>
</section>
<?php 

$sLinkToScript =  '<script src="js/get-loan.js"></script>';
require_once 'bottom.php';?>