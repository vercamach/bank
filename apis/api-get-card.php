<?php
ini_set('display_errors', 0);
session_start();

if( empty($_SESSION['sUserId'] ) ){
    sendResponse(-1, __LINE__, 'You must log in to use this api');
};

$sLoggedUser= $_SESSION['sUserId'];

$sCardType = $_POST['txtCardType'] ?? '';
if (empty($sCardType)) {sendResponse(-1, __LINE__, 'Card type missing');}

$sCardName = $_POST['txtCardName'] ?? '';
if (empty($sCardName)) {sendResponse(-1, __LINE__, 'Card name is misssing');}

$sData= file_get_contents('../data/clients.json');
$jData= json_decode($sData);
if($jData == null){sendResponse(0,__LINE__, 'Cannot convert the data');}
$jInnerData = $jData->data;
$jClient = $jInnerData->$sLoggedUser;

$jCardId=uniqid();
$jClient->cards->$jCardId= new stdClass();
$jClient->cards->$jCardId->id= $jCardId;
$jClient->cards->$jCardId->type= $sCardType;
$jClient->cards->$jCardId->name= $sCardName;
$jClient->cards->$jCardId->number = rand(1000000000000000,9999999999999999);
$jClient->cards->$jCardId->dayCreated= time();
$jClient->cards->$jCardId->active= 1;

$jInnerData->$sLoggedUser = $jClient;

$sData = json_encode($jData);
file_put_contents('../data/clients.json', $sData);

sendResponse(1,__LINE__, 'You have a new card');

function sendResponse($iStatus, $iLineNumber, $sMessage){
    echo '{"status":'.$iStatus.', "code":'.$iLineNumber.',"message":"'.$sMessage.'"}';
    exit;
  }