<?php
ini_set('display_errors', 0);

session_start();
if( empty($_SESSION['sUserId'] ) ){
    sendResponse(-1, __LINE__, 'You must login to use this api');
  };
$sLoggedUser= $_SESSION['sUserId'];

$sLoanName = $_POST['txtLoanType'] ?? '';
if (empty($sLoanName)) {sendResponse(-1, __LINE__, 'Loan name missing');}

$iAmount= $_POST['txtLoanAmount']?? '';
if(!isset($iAmount)){ sendResponse(0, __LINE__, 'Amount is missing'); }
if($iAmount<1){sendResponse(0, __LINE__, 'Amount must be 1 and higher');}
if( !ctype_digit($iAmount)  ){ sendResponse(0, __LINE__, 'Amount can only contain numbers');  }

$sData= file_get_contents('../data/clients.json');
$jData= json_decode($sData);
if($jData == null){sendResponse(0,__LINE__, 'Cannot convert the data');}
$jInnerData = $jData->data;
$jClient = $jInnerData->$sLoggedUser;

$jLoanId=uniqid();
$jClient->loans->$jLoanId= new stdClass();
$jClient->loans->$jLoanId->name= $sLoanName;
$jClient->loans->$jLoanId->id= $jLoanId;
$jClient->loans->$jLoanId->applicationDay= time();
$jClient->loans->$jLoanId->loanAmount = $iAmount;
$jClient->loans->$jLoanId->loanApproved = 0;

$jInnerData->$sLoggedUser = $jClient;

$sData = json_encode($jData);
file_put_contents('../data/clients.json', $sData);

sendResponse(1,__LINE__, 'You have applied for a loan');

function sendResponse($iStatus, $iLineNumber, $sMessage){
    echo '{"status":'.$iStatus.', "code":'.$iLineNumber.',"message":"'.$sMessage.'"}';
    exit;
  }
  