<?php

ini_set('user_agent', 'any');
ini_set('display_errors', 0);

session_start();
if( empty($_SESSION['sUserId'] ) ){
  sendResponse(-1, __LINE__, 'You must login to use this api');
}
$sLoggedInUser = $_SESSION['sUserId'];

$sData = file_get_contents('../data/clients.json');
$jData = json_decode( $sData );
if( $jData == null){ sendResponse(-1, __LINE__, 'Cannot convert data to JSON');  }
$jInnerData = $jData->data;

$sPhone = $_GET['phone'] ?? '';
if(empty($sPhone ) ){ sendResponse(0, __LINE__, 'Phone is missing'); }
if( strlen($sPhone) != 8 ){ sendResponse(0, __LINE__, 'Phone must be 8 characters in length'); }
if( !ctype_digit($sPhone)  ){ sendResponse(0, __LINE__, 'Phone can only contain numbers');  }

$iAmount = $_GET['amount'] ?? '';
if(!isset( $iAmount ) ){ sendResponse(0, __LINE__, 'Amount is missing'); }
if($iAmount <1 ){ sendResponse(0, __LINE__, 'Amount transfered has to be at least 1'); }
if( !ctype_digit($iAmount)  ){ sendResponse(0, __LINE__, 'Amount can only contain numbers');  }

$sMessage = $_GET['message'] ?? '';
if( empty( $sMessage) ){ sendResponse(0, __LINE__, 'Message is missing'); }
if( strlen($sMessage) <2 ){ sendResponse(0, __LINE__, 'Message must be at least 2 characters long'); }
if( strlen($sMessage) >20 ){ sendResponse(0, __LINE__, 'Message can have maximum 20 characters long'); }

if( !$jInnerData->$sPhone ){ 
  $jListOfBanks = fnjGetListOfBanksFromCentralBank();
  foreach( $jListOfBanks as $sKey => $jBank ){
    $sUrl = $jBank->url.'/apis/api-handle-transaction?phone='.$sPhone.'&amount='.$iAmount.'&message='.$sMessage; 
    $sBankResponse =  file_get_contents($sUrl);
    $jBankResponse = json_decode($sBankResponse);
    if( $jBankResponse->status == 1 && 
      $jBankResponse->code && 
      $jBankResponse->message ){ 
      sendResponse( 1, __LINE__ , $jBankResponse->message );
}
  }
  sendResponse( 0, __LINE__ , 'Phone does not exist' );
}

if($sLoggedInUser == $sPhone){  sendResponse( 0, __LINE__ , "You can't send money to yourself" );}
if($jInnerData->$sLoggedInUser->balance< $iAmount){sendResponse(0,__LINE__, "Balance on your account is too low");}
$jInnerData->$sPhone->balance += $iAmount;
$jInnerData->$sLoggedInUser->balance -=$iAmount;
$sTransactionUniqueId = uniqid();
$jTransaction->date = time();
$jTransaction->amount = $iAmount;
$jTransaction->fromPhone = $jInnerData->$sPhone->phone;
$jTransaction->message = $sMessage;

$jInnerData->$sPhone->transactions->$sTransactionUniqueId = $jTransaction;
$jInnerData->$sPhone->incomingTransactions->$sTransactionUniqueId = $jTransaction;
$jInnerData->$sLoggedInUser->transactions->$sTransactionUniqueId = $jTransaction;
$jInnerData->$sLoggedInUser->outgoingTransactions->$sTransactionUniqueId = $jTransaction;

$sData = json_encode($jData);

file_put_contents('../data/clients.json', $sData);

sendResponse( 1, __LINE__ , 'Transaction success within VERO bank'  );

function sendResponse($iStatus, $iLineNumber, $sMessage){
  echo '{"status":'.$iStatus.', "code":'.$iLineNumber.',"message":"'.$sMessage.'"}';
  exit;
}

function fnjGetListOfBanksFromCentralBank(){
  $sData = file_get_contents('https://ecuaguia.com/central-bank/api-get-list-of-banks.php?key=1111-2222-3333');
  return json_decode($sData);
}












