<?php

ini_set('user_agent', 'any');
ini_set('display_errors', 0);

session_start();
if (empty($_SESSION['sUserId'])) {
    sendResponse(-1, __LINE__, 'You must log in to use this api');
}
$sLoggedInUser = $_SESSION['sUserId'];

$sData = file_get_contents('../data/clients.json');
$jData = json_decode($sData);
if ($jData == null) {sendResponse(0, __LINE__, "Can't get the data");}
$jInnerData = $jData->data;

$sEmailReceiverPhone = $_POST['txtEmailReceiverPhone'] ?? '';
if (empty($sEmailReceiverPhone)) {sendResponse(0, __LINE__, 'Receivers phone missing');}
if (strlen($sEmailReceiverPhone) != 8) {sendResponse(0, __LINE__, 'Receivers phone must have 8 characters');}
if (!ctype_digit($sEmailReceiverPhone)) {sendResponse(0, __LINE__, 'Receivers phone can contain only digits');}

$sEmailSubject = $_POST['txtEmailSubject'] ?? '';
if (empty($sEmailSubject)) { sendResponse(0, __LINE__, 'Email subject is missing'); }
if (strlen($sEmailSubject) < 3) { sendResponse(0, __LINE__, 'Email subject is too short'); }
if (strlen($sEmailSubject) >20) { sendResponse(0, __LINE__, 'Email subject is too long'); }

$sEmailText = $_POST['txtEmailText'] ?? '';
if (empty($sEmailText)) { sendResponse(0, __LINE__, 'Email text is missing'); }
if (strlen($sEmailText)< 3) { sendResponse(0, __LINE__, 'Email is too short'); }
if (strlen($sEmailText)> 300) { sendResponse(0, __LINE__, 'Email is too long'); }

if(!$jInnerData->$sEmailReceiverPhone){ sendResponse(0,__LINE__,  'This phone is not registered in VERO bank');}
$sReceiverEmail = $jInnerData->$sEmailReceiverPhone->email;

if(mail($sReceiverEmail,$sEmailSubject, $sEmailText)){
    sendResponse(1,__LINE__, 'Your E-mail was sent');
}else{
    sendResponse(0, __LINE__, "Your E-mail couldn't be sent");
}
function sendResponse($iStatus, $iLineNumber, $sMessage)
{
    echo '{"status": '.$iStatus.', "code":'.$iLineNumber.', "message": "'.$sMessage.'" }';
    exit;
}
