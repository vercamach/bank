<?php

ini_set('user_agent', 'any');
ini_set('display_errors',0);

session_start();
if (!isset($_SESSION['sUserId'])) {
    header('Location: login');
    sendResponse(-1, __LINE__, 'You must login to use this api');
}

$sData = file_get_contents('../data/clients.json');
$jData = json_decode( $sData );
if( $jData == null){ sendResponse(-1, __LINE__, 'Cannot convert data to JSON');  }
$jInnerData = $jData->data;

$sPhone = $_GET['phone'] ?? '';
if (empty($sPhone)) {sendResponse(0, __LINE__, 'Phone missing');}
if (strlen($sPhone) != 8) {sendResponse(0, __LINE__, 'Phone must be 8 characters in length');}
if (!ctype_digit($sPhone)) {sendResponse(0, __LINE__, 'Phone can only contain numbers');}
if(!$jInnerData->$sPhone){sendResponse(0, __LINE__,  'Phone not registered in VERO bank');}

$iAmount = $_GET['amount'] ?? '';
if(!isset($iAmount) ){ sendResponse(0, __LINE__, 'Amount is missing'); }
if( !ctype_digit($iAmount)  ){ sendResponse(0, __LINE__, 'Amount can only contain numbers');  }
if($iAmount <1 ){ sendResponse(0, __LINE__, 'Amount transfered has to be at least 1'); }

$sMessage = $_GET['message'] ?? '';
if( empty( $sMessage) ){ sendResponse(0, __LINE__, 'Message is missing'); }
if( strlen($sMessage) <2 ){ sendResponse(0, __LINE__, 'Message is too short'); }
if( strlen($sMessage) >20 ){ sendResponse(0, __LINE__, 'Message is too long'); }

$jInnerData->$sPhone->balance += $iAmount;
$jTransaction= new stdClass();
$jTransaction->date = time();
$jTransaction->amount = $iAmount;
$jTransaction->fromPhone = 'bank admin';
$jTransaction->message = $sMessage;
$sTransactionUniqueId = uniqid();
$jInnerData->$sPhone->transactions->$sTransactionUniqueId = $jTransaction;
$jInnerData->$sPhone->incomingTransactions->$sTransactionUniqueId = $jTransaction;

$sData = json_encode($jData);
file_put_contents('../data/clients.json', $sData);

sendResponse(1, __LINE__, 'Transfer was successful');

function sendResponse($iStatus, $iLineNumber, $sMessage){
    echo '{"status":'.$iStatus.', "code":'.$iLineNumber.',"message":"'.$sMessage.'"}';
    exit;
  }
  
