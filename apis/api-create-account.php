<?php

ini_set('display_errors', 0);
session_start();
if( empty($_SESSION['sUserId'] ) ){
    sendResponse(-1, __LINE__, 'You must log in to use this api');
  };
$sLoggedUser= $_SESSION['sUserId'];

$sAccountName = $_POST['txtAccountName'] ?? '';
if (empty($sAccountName)) {sendResponse(-1, __LINE__, 'Account type missing');}

$sAccountCurrency = $_POST['txtSelectCurrency'] ?? '';
if (empty($sAccountCurrency)) {sendResponse(-1, __LINE__, 'Account currency is misssing');}

$sData= file_get_contents('../data/clients.json');
$jData= json_decode($sData);
if($jData == null){sendResponse(0,__LINE__, 'Cannot convert the data');}
$jInnerData = $jData->data;
$jClient = $jInnerData->$sLoggedUser;

$jAccountId=uniqid();
$jClient->accounts->$jAccountId= new stdClass();
$jClient->accounts->$jAccountId->id= $jAccountId;
$jClient->accounts->$jAccountId->name= $sAccountName;
$jClient->accounts->$jAccountId->dayOpened= time();
$jClient->accounts->$jAccountId->balance = rand(1,1000);
$jClient->accounts->$jAccountId->currency = $sAccountCurrency;

$jInnerData->$sLoggedUser = $jClient;

$sData = json_encode($jData);
file_put_contents('../data/clients.json', $sData);

sendResponse(1,__LINE__, 'You have created an account');

function sendResponse($iStatus, $iLineNumber, $sMessage){
    echo '{"status":'.$iStatus.', "code":'.$iLineNumber.',"message":"'.$sMessage.'"}';
    exit;
  }