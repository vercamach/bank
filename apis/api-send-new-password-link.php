<?php
ini_set('display_errors', 0);

$sForgotPasswordPhone= $_GET['resetPhone'] ?? '';
if(empty($sForgotPasswordPhone)){sendResponse(0,__LINE__, 'Phone is missing');}
if (!ctype_digit($sForgotPasswordPhone)) {sendResponse(0, __LINE__, 'Phone must contain only digits');}
if (strlen($sForgotPasswordPhone) != 8) {sendResponse(0, __LINE__, ' Phone has to be 8 characters long');}

$sData = file_get_contents('../data/clients.json');
$jData = json_decode($sData);
$jInnerData = $jData->data;

if(!$jInnerData->$sForgotPasswordPhone){sendResponse(0,__LINE__, 'Phone is not in  the database');}

$sPhone = $jInnerData->$sForgotPasswordPhone->phone;
$sKey = $jInnerData->$sForgotPasswordPhone->id;
$sEmail = $jInnerData->$sForgotPasswordPhone->email;
$sSubject = 'Password reset';

$sUrl = 'http://veru.dk/bank/update-forgotten-password?resetPasswordPhone='.$sPhone.'&resetPasswordKey='.$sKey;

if(!mail($sEmail,$sSubject,$sUrl)){
  sendResponse(0, __LINE__, 'Cannot sent email');}

sendResponse(1,__LINE__,' E-mail had been sent');

function sendResponse($iStatus, $iLineNumber, $sMessage){
    echo '{"status":'.$iStatus.', "code":'.$iLineNumber.',"message":"'.$sMessage.'"}';
    exit;
  }
 
