<?php
ini_set('user_agent', 'any');
ini_set('display_errors', 0);

session_start();
if( empty($_SESSION['sUserId'] ) ){
  sendResponse(-1, __LINE__, 'You must login to use this api');
}
$sLoggedInUser = $_SESSION['sUserId'];

$sData = file_get_contents('../data/clients.json');
$jData = json_decode( $sData );
if( $jData == null){ sendResponse(-1, __LINE__, 'Cannot convert data to JSON');  }
$jInnerData = $jData->data;

$sFromPhone = $_GET['phone'] ?? '';
if(empty($sFromPhone ) ){ sendResponse(0, __LINE__, 'Phone is missing'); }
if( strlen($sFromPhone) != 8 ){ sendResponse(0, __LINE__, 'Phone must be 8 characters long'); }
if( !ctype_digit($sFromPhone)  ){ sendResponse(0, __LINE__, 'Phone can only contain numbers');  }
if( !$jInnerData->$sFromPhone ){ sendResponse(0, __LINE__,'Phone not registered in Vero Bank');}
if($sLoggedInUser == $sFromPhone){  sendResponse( 0, __LINE__ , "You can't request money from your account" );}

$iAmount = $_GET['amount'] ?? '';
if(!isset($iAmount ) ){ sendResponse(0, __LINE__, 'Amount is missing'); }
if($iAmount < 1 ){ sendResponse(0, __LINE__, 'Amount requested has to be at least 1'); }
if( !ctype_digit($iAmount)  ){ sendResponse(0, __LINE__, 'Amount can contain only whole numbers');  }
if($jInnerData->$sFromPhone->balance <=1){sendResponse(0,__LINE__, "Balance on the requested account is too low");}
if($jInnerData->$sFromPhone->balance < $iAmount ){sendResponse(0,__LINE__, 'Balance on the requested account is too low');}

$sMessage = $_GET['message'] ?? '';
if( empty($sMessage) ){ sendResponse(0, __LINE__, 'Message is missing'); }
if( strlen($sMessage) <2 ){ sendResponse(0, __LINE__, 'Message can have maximum 20 characters'); }
if( strlen($sMessage) >10 ){ sendResponse(0, __LINE__, 'Message can have maximum 20 characters'); }

$jInnerData->$sFromPhone->balance -= $iAmount;
$jInnerData->$sLoggedInUser->balance += $iAmount;
$jTransaction->date = time();
$jTransaction->amount = $iAmount;
$jTransaction->fromPhone = $sFromPhone;
$jTransaction->message = $sMessage;
$sTransactionUniqueId = uniqid();
$jInnerData->$sLoggedInUser->incomingTransactions->$sTransactionUniqueId = $jTransaction;
$jInnerData->$sLoggedInUser->transactions->$sTransactionUniqueId = $jTransaction;
$jInnerData->$sFromPhone->transactions->$sTransactionUniqueId = $jTransaction;
$jInnerData->$sFromPhone->outgoingTransactions->$sTransactionUniqueId = $jTransaction;

$sData = json_encode($jData);
file_put_contents('../data/clients.json', $sData);

sendResponse( 1, __LINE__ , 'Money request successful'  );

function sendResponse($iStatus, $iLineNumber, $sMessage){
  echo '{"status":'.$iStatus.', "code":'.$iLineNumber.',"message":"'.$sMessage.'"}';
  exit;
}









