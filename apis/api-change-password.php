<?php
ini_set('display_errors', 0);
session_start();

if (!isset($_SESSION['sUserId'])) {
    header('Location: login');
}
$sUserId = $_SESSION['sUserId'];

$sData = file_get_contents('../data/clients.json');
$jData = json_decode($sData);
if ($jData == null) {echo 'Cannot get the data';}
$jInnerData = $jData->data;
$jClient = $jInnerData->$sUserId;

$sOldPassword = $_POST['txtOldPassword'] ?? '';
if (empty($sOldPassword)) {sendResponse(0, __LINE__, 'Old password missing');}
if (!password_verify($sOldPassword, $jClient->password)) {
    sendResponse(0, __LINE__, 'Wrong  old password');
}

$sNewPassword = $_POST['txtNewPassword'] ?? '';
if (empty($sNewPassword)) {sendResponse(0, __LINE__, 'New Password missing');}
if (strlen($sNewPassword) < 4) {sendResponse(0, __LINE__, 'New Password too short');}
if (strlen($sNewPassword) > 50) {sendResponse(0, __LINE__, 'New Password too long');}

$sNewConfirmPassword = $_POST['txtNewConfirmPassword'] ?? '';
if (empty($sNewConfirmPassword)) {sendResponse(0, __LINE__, 'Confirm password missing');}

if ($sNewPassword != $sNewConfirmPassword) {sendResponse(0, __LINE__, "Passwords don't match");}
$jClient->password = password_hash($sNewConfirmPassword, PASSWORD_DEFAULT);

$sData = json_encode($jData);
file_put_contents('../data/clients.json', $sData);
sendResponse(1, __LINE__, 'Password was updated');

function sendResponse($iStatus, $iLineNumber, $sMessage){
    echo '{"status": '.$iStatus.', "code":'.$iLineNumber.', "message": "'.$sMessage.'" }';
    exit;
}
