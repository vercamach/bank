<?php
ini_set('display_errors', 0);
session_start();

if( !isset($_SESSION['sUserId'] ) ){ sendResponse(0,__LINE__, 'User not logged in');}
if (empty($_GET['cardId'])) { sendResponse(0,__LINE__, 'Card ID is missing');}
if (empty($_GET['phone'])){ sendResponse(0,__LINE__, 'Phone is missing');}
$sCardId = $_GET['cardId'];
$sClientPhone = $_GET['phone'];

$sData = file_get_contents('../data/clients.json');
$jData = json_decode($sData);
if ($jData == null) {echo 'data corrupted';}
$jInnerData = $jData->data;

if(!$jInnerData->$sClientPhone->cards->$sCardId->id){
    sendResponse(0,__LINE__, "ID doesn't match");
    header('Location: ../profile');
}
$jInnerData->$sClientPhone->cards->$sCardId->active = !$jInnerData->$sClientPhone->cards->$sCardId->active;

$sData = json_encode($jData);
file_put_contents('../data/clients.json', $sData);
header('Location: ../cards');
if($jInnerData->$sClientPhone->cards->$sCardId->active==1){sendResponse(1,__LINE__, 'Card had beed activated');}
if($jInnerData->$sClientPhone->cards->$sCardId->active==0){sendResponse(1,__LINE__, 'Card had beed cancelled');}

function sendResponse($iStatus, $iLineNumber, $sMessage){
    echo '{"status":'.$iStatus.', "code":'.$iLineNumber.',"message":"'.$sMessage.'"}';
    exit;
}
     
    