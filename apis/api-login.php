<?php

ini_set('display_errors', 0);

$sData = file_get_contents('../data/clients.json');
$jData = json_decode($sData);
if ($jData == null) {sendResponse(0, __LINE__, "Can't get the data");}
$jInnerData = $jData->data;

$sPhone = $_POST['txtLoginPhone'];
if (empty($sPhone)) {sendResponse(0, __LINE__, 'Phone missing');}
if (strlen($sPhone) != 8) {sendResponse(0, __LINE__, 'Phone must have 8 characters');}
if (!ctype_digit($sPhone)) {sendResponse(0, __LINE__, 'Phone can contain only digits');}
if ($sPhone != $jInnerData->$sPhone->phone) {sendResponse(0, __LINE__, "This phone isn't in the database");}

$sPassword = $_POST['txtLoginPassword'];
if (empty($sPassword)) {sendResponse(0, __LINE__, 'Password missing');}

if ($jInnerData->$sPhone->active != 1) {sendResponse(0, __LINE__, 'Email is not activated');}

if ($jInnerData->$sPhone->loginAttemptsLeft == 0) {
    $iSecondsElapsedsSinceLastLogin = $jInnerData->$sPhone->lastLoginAttemptTime + 5 - time();
    if ($iSecondsElapsedsSinceLastLogin <= 0) {
        if (!password_verify($sPassword, $jInnerData->$sPhone->password)) {
            $jInnerData->$sPhone->lastLoginAttemptTime = time();
            $iSecondsElapsedsSinceLastLogin = $jInnerData->$sPhone->lastLoginAttemptTime + 5 - time();
            $sData = json_encode($jData);
            file_put_contents('../data/clients.json', $sData);
            sendResponse(0, __LINE__, "You have to wait another {$iSecondsElapsedsSinceLastLogin} seconds to log in");
        } else {
            $jInnerData->$sPhone->loginAttemptsLeft = 3;
            $jInnerData->$sPhone->lastLoginAttemptTime = 0;
            $sData = json_encode($jData);
            file_put_contents('../data/clients.json', $sData);
            session_start();
            $_SESSION['sUserId'] = $sPhone;
            sendResponse(1, __LINE__, 'User logged in');
        }
    }
    sendResponse(0, __LINE__, "Wait {$iSecondsElapsedsSinceLastLogin} seconds and try to log in again");
}
if (!password_verify($sPassword, $jInnerData->$sPhone->password)) {
    $jInnerData->$sPhone->loginAttemptsLeft--;
    $jInnerData->$sPhone->lastLoginAttemptTime = time();
    $sData = json_encode($jData);
    file_put_contents('../data/clients.json', $sData);
    sendResponse(0, __LINE__, "You have {$jInnerData->$sPhone->loginAttemptsLeft} attempts left");
}

session_start();
$_SESSION['sUserId'] = $sPhone;

sendResponse(1, __LINE__, 'User logged in');

function sendResponse($iStatus, $iCode, $sMessage){
    echo '{"status": ' . $iStatus . ', "code":' . $iCode . ', "message": "' . $sMessage . '"}';
    exit;
}
