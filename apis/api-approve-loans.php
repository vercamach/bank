<?php
session_start();
if (!isset($_SESSION['sUserId'])) {
    header('Location : ../login');
   sendResponse(-1, __LINE__, 'You have to be logged in');
}
if (!isset($_GET['id'])) {
    header('Location: ../admin-profile');
    sendResponse(0, __LINE__, 'Loans ID is missing');
}
if (!isset($_GET['phone'])) {
    header('Location: ../admin-profile');
    sendResponse(0, __LINE__, 'Users phone is missing');
}
if (!isset($_GET['amount'])) {
    header('Location: ../admin-profile');
    sendResponse(0, __LINE__, 'Amount phone is missing');
}

$sLoanId = $_GET['id'];
$iAmount = $_GET['amount'];
$sClientPhone = $_GET['phone'];

$sData = file_get_contents('../data/clients.json');
$jData = json_decode($sData);
if ($jData == null) {
    sendResponse(-1, __LINE__, 'Data are corrupted');
}

$jInnerData = $jData->data;
if ($jInnerData->$sClientPhone->loans->$sLoanId->id != $sLoanId) {
    sendResponse(-1, __LINE__, "Loan id doesn't match any loan in the database");
    header('Location : ../admin-profile');
}

$jInnerData->$sClientPhone->loans->$sLoanId->loanApproved = ! $jInnerData->$sClientPhone->loans->$sLoanId->loanApproved;
if($jInnerData->$sClientPhone->loans->$sLoanId->loanApproved ==1){$jInnerData->$sClientPhone->balance += $iAmount;}

$sData = json_encode($jData);
file_put_contents('../data/clients.json', $sData);
header('Location: ../admin-profile');

function sendResponse($iStatus, $iCode, $sMessage){
    echo '{"status" : '.$iStatus.', "code" : '.$iCode.', "message" : "'.$sMessage.'"}';
    exit;
}