<?php
ini_set('display_errors', 0);

$sResetPasswordPhone = $_POST['txtResetPasswordPhone'];
if(empty($sResetPasswordPhone)){sendResponse(0,__LINE__, 'Missing reset password phone');}
if (strlen($sResetPasswordPhone) != 8) {sendResponse(0, __LINE__, 'Phone has to be 8 characters long');}
if (!ctype_digit($sResetPasswordPhone)) {sendResponse(0, __LINE__, 'Phone must contain only digits');}

if(empty($_POST['txtNewResetPassword'])){sendResponse(0,__LINE__, 'Missing new reset password');}
$sNewResetPassword = $_POST['txtNewResetPassword'];
if (strlen($sNewResetPassword) < 4) {sendResponse(0, __LINE__, 'Password too short');}
if (strlen($sNewResetPassword) > 50) {sendResponse(0, __LINE__, 'Password too long');}

if(empty($_POST['txtNewConfirmPassword'])){sendResponse(0,__LINE__, 'Missing confirm reset password');}
$sNewResetConfirmPassword = $_POST['txtNewConfirmPassword'];
if (empty($sNewResetConfirmPassword)) {sendResponse(0, __LINE__, 'Confirm password missing');}
if ($sNewResetPassword != $sNewResetConfirmPassword) {sendResponse(0, __LINE__, "Passwords don't match");}

$sData = file_get_contents('../data/clients.json');
$jData= json_decode($sData);
$jInnerData = $jData->data;

$jInnerData->$sResetPasswordPhone->password = password_hash($sNewResetPassword, PASSWORD_DEFAULT);

$sData = json_encode($jData);
file_put_contents('../data/clients.json', $sData);
sendResponse(1, __LINE__, 'Password had been updated');

function sendResponse($iStatus, $iCode, $sMessage){
    echo '{"status": '.$iStatus.', "code":'.$iCode.', "message": "'.$sMessage.'"}';
    exit;
}
