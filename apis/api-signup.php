<?php
ini_set('display_errors', 0);


$sName = $_POST['txtSignupName'] ?? '';
if (empty($sName)) {sendResponse(0, __LINE__, 'Name missing');}
if (strlen($sName) < 2) {sendResponse(0, __LINE__, 'Name too short');}
if (strlen($sName) > 10) {sendResponse(0, __LINE__, 'Name too long');}

$sLastName = $_POST['txtSignupLastName'] ?? '';
if (empty($sLastName)) {sendResponse(0, __LINE__, 'Lase Name missing');}
if (strlen($sLastName) < 2) {sendResponse(0, __LINE__, 'Last Name too short');}
if (strlen($sName) > 10) {sendResponse(0, __LINE__, 'Last Name too long');}

$sPhone = $_POST['txtSignupPhone'] ?? '';
if (empty($sPhone)) {sendResponse(0, __LINE__, 'Phone missing');}
if (strlen($sPhone) != 8) {sendResponse(0, __LINE__, ' Phone has to be 8 characters long');}
if (!ctype_digit($sPhone)) {sendResponse(-1, __LINE__, 'Phone must contain only digits');}

$sEmail = $_POST['txtSignupEmail'] ?? '';
if (empty($sEmail)) {sendResponse(0, __LINE__, 'Email missing');}
if (strlen($sEmail) < 5) {sendResponse(0, __LINE__, 'Email too short');}
if (strlen($sEmail) > 40) {sendResponse(0, __LINE__, 'Email too long');}
if (!filter_var($sEmail, FILTER_VALIDATE_EMAIL)) {sendResponse(0, __LINE__, 'Not a valid Email');}

$sPassword = $_POST['txtSignupPassword'] ?? '';
if (empty($sPassword)) {sendResponse(0, __LINE__, 'Password missing');}
if (strlen($sPassword) < 4) {sendResponse(0, __LINE__, 'Password too short');}
if (strlen($sPassword) > 50) {sendResponse(0, __LINE__, 'Password too long');}

$sConfirmPassword = $_POST['txtSignupConfirmPassword'] ?? '';
if (empty($sConfirmPassword)) {sendResponse(0, __LINE__, 'Confirm password missing');}
if ($sPassword != $sConfirmPassword) {sendResponse(0, __LINE__, "Passwords don't match");}

$sData = file_get_contents('../data/clients.json');
$jData = json_decode($sData);
if ($jData == null) {sendResponse(0, __LINE__, 'Data not found');}
$jInnerData = $jData->data;

$jClient = new stdClass();

$jClient->id = uniqid();
$jClient->name = $sName;
$jClient->lastName = $sLastName;
$jClient->phone = $sPhone;
$jClient->email = $sEmail;
$jClient->password = password_hash($sPassword, PASSWORD_DEFAULT);
$jClient->balance = rand(10, 1000);

$jClient->loginAttemptsLeft=3;
$jClient->lastLoginAttemptTime=0;

$sActivationKey = uniqid();
$jClient->activationKey = $sActivationKey;
$jClient->active=0;
$jClient->blocked = 0;

$jClient->transactions = new stdClass();
$jClient->outgoingTransactions= new stdClass();
$jClient->incomingTransactions= new stdClass();

$jClient->accounts = new stdClass();
$jClient->cards = new stdClass();
$jClient->loans = new stdClass();

$jInnerData->$sPhone = $jClient;

$sUrl = 'http://veru.dk/bank/apis/api-authentication-email?activatePhone='.$sPhone.'&activateKey='.$sActivationKey;

$sData = json_encode($jData);
if ($sData == null) {sendResponse(0, __LINE__, "No data detected");}
file_put_contents('../data/clients.json', $sData);

$sSubject = 'activation';
if(! mail($sEmail,$sSubject,$sUrl)){sendResponse(0,__LINE__,"Could't send e-mail");}

sendResponse(1, __LINE__, "Client signed up");

function sendResponse($iStatus, $iLineNumber, $sMessage){
    echo '{"status": '.$iStatus.', "code":'.$iLineNumber.', "message": "'.$sMessage.'" }';
    exit;
}
