<?php

ini_set('user_agent', 'any');
ini_set('display_errors', 0);

session_start();

if (empty($_SESSION['sUserId'])) {
    sendResponse(-1, __LINE__, 'You must login to use this api');}
$sLoggedInUser = $_SESSION['sUserId'];

$sData = file_get_contents('../data/clients.json');
$jData = json_decode($sData);
if ($jData == null) {sendResponse(0, __LINE__, "Can't get the data");}
$jInnerData = $jData->data;

$sEmailAllSubject = $_POST['txtEmailAllSubject'] ?? '';
if (empty($sEmailAllSubject)) {sendResponse(0, __LINE__, 'Email subject is missing');}
if (strlen($sEmailAllSubject) < 2) {sendResponse(0, __LINE__, 'Email subject is too short');}
if(strlen($sEmailAllSubject) >15){sendResponse(0,__LINE__, 'Email subject is too long');}

$sEmailAllText = $_POST['txtEmailAllText'] ?? '';
if (empty($sEmailAllText)) {sendResponse(0, __LINE__, 'Email text is missing');}
if (strlen($sEmailAllText) < 2) {sendResponse(0, __LINE__, 'Email text is too short');}
if (strlen($sEmailAllText) > 200) {sendResponse(0, __LINE__, 'Email text is too long');}

foreach ($jInnerData as $sKey => $jClient) {
    $sEmailReceiver = $jClient->email;
    if(!mail($sEmailReceiver, $sEmailAllSubject, $sEmailAllText)){
        sendResponse(0, __LINE__, "E-mail couldn't be sent");
        };
}
sendResponse(1, __LINE__, "Emails were sent");

function sendResponse($iStatus, $iLineNumber, $sMessage){
    echo '{"status": '.$iStatus.', "code":'.$iLineNumber.', "message": "'.$sMessage.'" }';
    exit;
}
