<?php
ini_set('display_errors', 0);
session_start();
if( !isset($_SESSION['sUserId'] ) ){
  header('Location: ../login');
}
if (!isset($_GET['id'])) {
    header('Location: ../admin-profile');
}
if (!isset($_GET['phone'])) {
    header('Location: ../admin-profile');
}
$sClientId = $_GET['id'];
$sClientPhone = $_GET['phone'];

$sData = file_get_contents('../data/clients.json');
$jData = json_decode($sData);
if ($jData == null) {
    echo 'data corrupted';
}
$jInnerData = $jData->data;

if($jInnerData->$sClientPhone->id!= $sClientId){
    echo 'ID does not match';
    header('Location: ../admin-profile');
}
    
 $jInnerData->$sClientPhone->blocked = !  $jInnerData->$sClientPhone->blocked;
    
$sData = json_encode($jData);
file_put_contents('../data/clients.json', $sData);
  
header('Location: ../admin-profile');


