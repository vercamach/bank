<?php

ini_set('display_errors', 0);

$sData = file_get_contents('../data/clients.json');
$jData = json_decode($sData);
if ($jData == null) {fnvSendResponse(-1, __LINE__, 'Cannot convert the data file to json');}
$jInnerData = $jData->data;

$sPhoneFromOtherServer = $_GET['phone'] ?? '';
if (empty($_GET['phone'])) {sendResponse(0, __LINE__, 'Phone missing');}
if (strlen($sPhoneFromOtherServer) != 8) {sendResponse(0, __LINE__, 'Phone must be 8 characters in length');}
if (!ctype_digit($sPhoneFromOtherServer)) {sendResponse(0, __LINE__, 'Phone can only contain numbers');}

$iAmountFromOtherServer = $_GET['amount'] ?? '';
if (!ctype_digit($iAmountFromOtherServer)) {sendResponse(0, __LINE__, 'Amount can only contain numbers');}
if ($iAmountFromOtherServer < 1) {sendResponse(0, __LINE__, 'Amount transfered has to be at least 1');}

$sMessageFromOtherServer = $_GET['message'];

if (!$jInnerData->$sPhoneFromOtherServer) {
    fnvSendResponse(0, __LINE__, 'Phone not registered in VERO Bank');
}

$jInnerData->$sPhoneFromOtherServer->balance += $iAmountFromOtherServer;
$sTransactionUniqueId = uniqid();
$jTransaction=new stdClass();
$jTransaction->date = time();
$jTransaction->amount = $iAmountFromOtherServer;
$jTransaction->fromPhone = $sPhoneFromOtherServer;
$jTransaction->message = $sMessageFromOtherServer;
$jInnerData->$sPhoneFromOtherServer->transactions->$sTransactionUniqueId = $jTransaction;



$sData = json_encode($jData);
file_put_contents('../data/clients.json', $sData);

fnvSendResponse(1, __LINE__, 'Transaction success.');

function fnvSendResponse( $iStatus, $iLineNumber, $sMessage ){
  echo '{"status":'.$iStatus.', "code":'.$iLineNumber.', "message":"'.$sMessage.'"}';
  exit;
}