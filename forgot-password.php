<?php
require_once 'top-basic.php'?>
<section>
  <div class="form-wrapper">
    <h1 class="title">Reset password</h1>
    <form id="frmForgotPasswordSendLink" action="apis/api-send-new-password-link" method="GET">
    <div>
    <img class="loginProfile" src="img/phone.png" alt="icon">
      <input name="txtForgotPasswordPhone" id="txtForgotPasswordPhone" type="text" placeholder="phone"
      data-validate="yes" data-type="string" data-min="8" data-max="8">
</div>
      <button>Send E-mail</button>
    </form>
    <div>
      <a href="login" class="centerLink backToLoginLink">
        <- back to login 
      </a>
    </div>
  </div>
</section>
<?php
$sLinkToScript = '<script src="js/forgot-password.js"></script>';
require_once 'bottom.php'
?>