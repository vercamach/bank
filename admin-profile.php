<?php
ini_set('display_errors', 0 );

session_start();
if( !isset($_SESSION['sUserId'] ) ){
  header('Location: login');
}
$sUserId = $_SESSION['sUserId'];
$sData = file_get_contents('data/clients.json');
$jData = json_decode($sData);
if ($jData == null) {echo 'System update';}
$jInnerData = $jData->data;
$jClient = $jInnerData->$sUserId;
require_once 'top-admin.php';
?>
<div class="admin-wrapper">
    <section class="active page" id="viewCustomers">
        <div class="admin-profile">
            <h1 class="title">View customers</h1>
                        <!--        *********** CLIENTS and LOANS ***************      -->
<?php
$sData = file_get_contents('data/clients.json');
// echo $sData;
$jData = json_decode($sData);
if ($jData == null) { 
    echo 'Data cannot be transformed';
    exit;
}
$jInnerData = $jData->data;
foreach ($jInnerData as $sKey => $jClient) {
    json_encode($jClient, JSON_PRETTY_PRINT);
    $sIsBlockedOrNot = $jClient->blocked == 1 ? 'ACTIVATE' : 'BLOCK';
    $sStatus = $jClient->blocked == 1 ? 'false' : 'true';
    echo "
                <div class='client'>
                    <div class='profile-flex'>
                        <div class='admin tab person' id='admin'>
                            <img class='user' src='img/user-1.png' alt='user'>
                            <div  class='profileDiv' >
                                <h4>ID: $jClient->id</h4>
                                <h4>NAME: $jClient->name</h4>
                                 <h4>LAST NAME: $jClient->lastName</h4>
                                 <h4>PHONE: $jClient->phone</h4>
                                 <h4>E-MAIL: $jClient->email</h4>
                                 <h4>BALANCE: $jClient->balance</h4>
                                 <h4> STATUS:
                                     <span class='$sStatus dot'></span>
                                     <a class='toggleLink' href='apis/api-block-or-unblock-client?id={$jClient->id}&phone={$jClient->phone}'>$sIsBlockedOrNot</a>
                                 </h4>
                            </div>
                        </div>
                    </div>
                </div>
                <div class='tab-admin'>
                    <h1 class='tab-title tab-title-admin'>Loans</h1>
                     <table class='admin'>
                        <thead>
                            <tr>
                                <td>Name</td>
                                <td>Amount</td>
                                <td>Status</td>
                                <td></td>
                            </tr>
                        </thead>
                        <tbody id='lblTransactions'>";
       $jLoans = $jClient->loans;
       foreach ($jLoans as $sLoanKey => $jLoan) {
            $sStatus = $jLoan->loanApproved == 0 ? 'false' : 'true';
            $sIsApproved = $jLoan->loanApproved == 0 ? 'APPROVE' : 'DECLINE';
            echo "
                            <tr>
                                <td>$jLoan->name</td>
                                <td>$jLoan->loanAmount DKK</td>
                                <td> <span class='dot $sStatus'></span></td>
                                <td>
                                    <a class='toggleLink' href='apis/api-approve-loans?id={$jLoan->id}&phone={$jClient->phone}&amount={$jLoan->loanAmount}'>$sIsApproved</a>
                                </td>
                            </tr>";
    }
    echo"
                        </tbody>
                    </table>
                </div>";
}
?>
        </div>
    </section>

                        <!--        *********** SEND EMAIL TO ONE ***************      -->

    <section class="not-visible-on-load page admin" id="sendEmail">
        <div class="form-wrapper admin-form">
            <h1>Send E-mail</h1>
            <form id="adminSendEmail" method: "POST">
                <input id="txtEmailReceiverPhone" name="txtEmailReceiverPhone" type="text" placeholder="receivers phone"
                data-validate="yes" data-type="string"  data-min="8" data-max="8"  >
                <input id="txtEmailSubject" name="txtEmailSubject" type="text" placeholder="subject"
                data-validate="yes" data-type="string" data-min="3" data-max="20" >
                <input id="txtEmailText" name="txtEmailText" type="text" placeholder="text"
                data-validate="yes" data-type="string"  data-min="3" data-max="300" >
                <button>Send</button>
            </form>
         </div>
    </section>

                        <!--        *********** SEND EMAIL TO ALL***************      -->
    <section class="not-visible-on-load page" id="contactAll">
        <div class="form-wrapper admin-form">
            <h1>Contact all clients</h1>
            <form id="adminContactAll" method: "POST">
                <input id="txtEmailAllSubject" name="txtEmailAllSubject" type="text" placeholder="email subject"
                data-validate="yes" data-min=2 data-max=15 data-type="string">
                <input id="txtEmailAllText" name="txtEmailAllText" type="text" placeholder="email text"
                data-validate="yes" data-min="2" data-max="200" data-type="string">
                <button>Send</button>
            </form>
        </div>
    </section>


                        <!--        ***********   TRANSFER MONEY TO ACCOUNT   ***************      -->
    <section class="not-visible-on-load page" id="transferMoney">
        <div class="form-wrapper admin-form">
            <h1>Transfer money</h1>
            <form id="adminTransfer" method: "GET">
                <input id="txtTransferTo" name="txtTransferTo" type="text" placeholder="phone"
                data-validate="yes" data-min="8" data-max="8" data-type="string" >
                <input id="txtTransferAmount" name="txtTransferAmount" type="number" placeholder="amount"
                data-validate="yes" data-min="1" data-type="integer">
                <input id="txtTransferMessage" name="txtTransferMessage" type="text" placeholder="message"
                data-validate="yes" data-min="2" data-max="20" data-type="string">
                <button>Send</button>
            </form>
        </div>
    </section>
</div>
<?php
$sLinkToScript = '<script src="js/admin-profile.js"></script>';
require_once 'bottom.php';
?>